CREATE DATABASE todo_list WITH ENCODING='UTF8' OWNER=postgres;
CREATE TYPE priority_type AS ENUM ('Fácil', 'Médio', 'Difícil');
CREATE TABLE tasks(
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(160),
    priority priority_type
);

